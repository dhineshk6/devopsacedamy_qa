package TestCases;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;

import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)

@CucumberOptions(

      features="src/test/java/FeatureFile/FeatureFile.feature",

      format={"pretty","html:target/Reports"},
      glue="classpath:"

      )

public class TestRunner_Cucumber {

}
