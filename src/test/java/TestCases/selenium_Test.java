package TestCases;

import static org.junit.Assert.assertTrue;

import java.net.MalformedURLException;
import java.net.URL;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
//import org.testng.Assert;
//import org.testng.annotations.AfterTest;
//import org.testng.annotations.BeforeTest;

import ObjectRepository.HomePage_OR;




public class selenium_Test {

	protected WebDriver driver;	
	// Create a new instance of the chrome driver
	/*//for local
	 @Before
	 public void setUp() throws MalformedURLException {
		 String chromePath = "D:\\Users\\703217907\\Documents\\chromedriver.exe";
			
		 System.setProperty("webdriver.chrome.driver", chromePath);
		 driver = new ChromeDriver();
		 		 
		   DesiredCapabilities dr=null;
		    dr=DesiredCapabilities.chrome();
		   
	     //  if(browserType.equals("firefox"))
	      // {
	    	//   dr=DesiredCapabilities.firefox();
	    	//   dr.setBrowserName("firefox");
	    //   }
	     //  if(browserType.equals("chrome"))
	     //  {
	    	  dr=DesiredCapabilities.chrome();
	    	 // dr.setBrowserName("chrome");
	    //   }
	      // dr.setPlatform(Platform.WINDOWS);
	   
	   //    driver=new RemoteWebDriver(new URL("http://52.90.151.23:4444/wd/hub"), dr);
	      
		   
	        //DesiredCapabilities dc = DesiredCapabilities.chrome();

	       // if (System.getProperty("browser").equals("firefox"))
	       
	    }	*/
	
 @Before //for remote
 public void setUp() throws MalformedURLException {
	 
	// driver = new ChromeDriver();
	 
	 
	   DesiredCapabilities dr=null;
     //  if(browserType.equals("firefox"))
      // {
    	//   dr=DesiredCapabilities.firefox();
    	//   dr.setBrowserName("firefox");
    //   }
     //  if(browserType.equals("chrome"))
     //  {
    	  dr=DesiredCapabilities.chrome();
    	 // dr.setBrowserName("chrome");
    //   }
      // dr.setPlatform(Platform.WINDOWS);
   
       driver=new RemoteWebDriver(new URL("http://52.90.151.23:4444/wd/hub"), dr);
      
	  
    }	
@Test
public void verifyHomePageTest() 
{
  //Testcase1 : To verify the elements at home page
  
 // System.setProperty("webdriver.chrome.driver", chromePath);
 // WebDriver driver = new ChromeDriver();
 HomePage_OR home=new HomePage_OR(); 
 // driver.get("http://localhost:9090/demo/#/");
  driver.get("http://35.174.137.193:9090/demo/#/");
  // Print a Log In message to the screen
  System.out.println("Successfully opened the demo App");

	//Wait for 5 Sec
	try {
		Thread.sleep(2000);
	} catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	if (!driver.findElement(home.txt_UserName()).getText().isEmpty())
		System.out.println("Username feild should be blank by default");
	if (!driver.findElement(home.txt_Age()).getText().isEmpty())
		System.out.println("Age feild should be blank by default");
	if (!driver.findElement(home.txt_Salary()).getText().isEmpty())
		System.out.println("Salary feild should be blank by default");
	if (!driver.findElement(home.btn_Reset()).isEnabled())
		System.out.println("reset button should be enabled by default");
	if (!driver.findElement(home.btn_Add()).isDisplayed())
		System.out.println("Add button should be displayed by default");
	 System.out.println("Successfully Verified home Page elements");
/*	try {
		Thread.sleep(2000);
	} catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}*/
	//driver.wait("5000");
  // Close the driver
  
}
@Test
public void VerifyResetButtonTest()
{
  HomePage_OR home=new HomePage_OR(); 
	 // driver.get("http://localhost:9090/demo/#/");
	  driver.get("http://35.174.137.193:9090/demo/#/");
      // Print a Log In message to the screen
      System.out.println("Successfully opened the demo App");

		//Wait for 5 Sec
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		driver.findElement(home.txt_UserName()).sendKeys("test1");
		driver.findElement(home.txt_Age()).sendKeys("12");
		driver.findElement(home.txt_Salary()).sendKeys("23000");
		
		driver.findElement(home.btn_Reset()).click();
		
		assertTrue("Username feild should be blank", driver.findElement(home.txt_UserName()).getText().isEmpty());
		assertTrue("Age feild should be blank", driver.findElement(home.txt_Age()).getText().isEmpty());
		assertTrue("Salary feild should be blank", driver.findElement(home.txt_Salary()).getText().isEmpty());
				
	/*	try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		//driver.wait("5000");
      // Close the driver
      //driver.quit();
}

@After
public void quitDriver()
{
  driver.quit();
}
	
	

}
